$(document).on('ready', function() {	
	$(document).on('pjax:beforeSend', function(event) {
		$(event.target).addClass('pjax loading');
	});
	
	$(document).pjax('a[data-pjax]');
	
	$(document).on('submit', 'form[data-pjax]', function(event) {
		$.pjax.submit(event, $(this).data('pjax'));
	});
	
	$(document).on('pjax:success', function(event) {
		$(event.target).removeClass('pjax loading');
	});
	
	$(document).on('pjax:timeout', function(event) {
		return false;
	});
});

<?php
    function video_id( $html ) {
        $seekers = array(
            'youtube', 'youtube_preview', 'youtube_iframe', 'youtube_video'
        );
        $videoID = false;
        
        foreach ($seekers as $seeker) {            
            if ( $videoID )
                break;
            
            $videoID = call_user_func($seeker, $html);
        }
        
        return $videoID;
    }
    
    function youtube( $html ) {
        $matches = array();
        preg_match("/youtube.com\?v=([\w\d-_]*)/i", $html, $matches);
        
        return isset($matches[1]) ? $matches[1] : false;
    }
    
    function youtube_preview( $html ) {
        $matches = array();
        preg_match("/img.youtube.com\/vi\/([\w\d-_]*)\/[0..9]*.(jpg|png|gif)/i", $html, $matches);
        
        return isset($matches[1]) ? $matches[1] : false;
    }
    
    function youtube_iframe( $html ) {
        $matches = array();
        preg_match("/\/\/www.youtube.com\/embed\/([\w\d-_]*)\?/i", $html, $matches);
        
        return isset($matches[1]) ? $matches[1] : false;
    }
    
    function youtube_video( $html ) {
        $matches = array();
        preg_match("/\?v=([\w\d-_]*)/i", $html, $matches);
        
        return isset($matches[1]) ? $matches[1] : false;
    }
    
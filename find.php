<?php
    if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) {
        header('Location: http://manage-expenses.com/video-unlocker/');
        exit;
    }
?>

<?php if ( !isset($_REQUEST['url']) || !$_REQUEST['url'] ): ?>
    <div class="alert alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Please enter video url.</strong>
    </div>
    <?php exit(); ?>
<?php endif; ?>

<?php
    require_once('finders.php');
    error_reporting(0);
    
    $url = $_REQUEST['url'];
    
    if ( strpos($url, 'http://') === false ) {
        $url = 'http://' . $_REQUEST['url'];
    }
    
    $foundInDB = false;
    $videID = false;
    $html = '';
    $matches = array();
    
    # first search for video id in db
    $db = new SQLite3('queries.db');
    $statement = $db->prepare('SELECT video_id FROM urls WHERE url = :url;');
    $statement->bindValue(':url', $url);
    $results = $statement->execute();
    
    while ($row = $results->fetchArray()) {
        $videID = $row['video_id'];
        $foundInDB = true;
    }
    
    # not found in db
    # search for video id in page source
    if ( !$foundInDB ) {
        try {
            $html = file_get_contents($url);
        } catch(Exception $e) {}
        
        $videID = video_id( $html );
    
        if ( $videID ) {
            # video id found in page source
            # insert url and video id in db
            $statement = $db->prepare('INSERT INTO urls (url, video_id, created) VALUES (:url, :video_id, :created);');
            $statement->bindValue(':url', $url);
            $statement->bindValue(':video_id', $videID);
            $statement->bindValue(':created', date('Y-m-d H:i:s'));
            
            $results = $statement->execute();
        } else {
            # video id not found in page source
            
            # first search for url in not found table
            $statement = $db->prepare('SELECT url FROM urls_not_found WHERE url = :url;');
            $statement->bindValue(':url', $url);
            $results = $statement->execute();
            
            $exists = false;
            while ($row = $results->fetchArray()) {
                $exists = true;
            }
            
            if ( !$exists ) {
                # insert url in not found table
                $statement = $db->prepare('INSERT INTO urls_not_found (url, created) VALUES (:url, :created);');
                $statement->bindValue(':url', $url);
                $statement->bindValue(':created', date('Y-m-d H:i:s'));
                
                $results = $statement->execute();
            }
        }
    }
?>

<?php if ( $videID ): ?>
    <div class="alert alert-success fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <a href="//youtube.com/watch?v=<?php echo $videID; ?>">View on youtube.com</a>
    </div>
    
    <iframe width="100%" height="100%" src="//www.youtube.com/embed/<?php echo $videID; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
<?php else: ?>   
    <div class="alert alert-warning fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Can&rsquo;t find video.</strong>Try again with different url.
    </div>
<?php endif; ?>

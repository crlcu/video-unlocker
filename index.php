<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Stop sharing stupid videos on Facebook - unlock them smartly" />
    <meta name="author" content="Adrian Crisan" />
    
    <meta property="og:title" content="Smart video unlocker" /> 
    <meta property="og:description" content="Stop sharing stupid videos on Facebook - unlock them smartly" />
    <meta property="og:image" content="http://manage-expenses.com/video-unlocker/assets/images/screen.png" /> 
    
    <title>Smart video unlocker</title>
    
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />

    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script type="text/javascript" src="assets/pjax/jquery.pjax.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/pjax/pjax.js"></script>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="//manage-expenses.com/video-unlocker">Smart video unlocker</a>
            </div>
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://manage-expenses.com">by manage-expenses.com</a></li>
            </ul>
        </div>
    </div>
    
    <div class="push"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form action="find.php" method="post" data-pjax="#result .video" data-pjax-push="false">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="url" class="form-control" id="url" autofocus="autofocus" autocomplete="off" placeholder="Enter here the address that asks you to give share/like to see the video" title="Enter here the address that asks you to give share/like to see the video" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div id="result" class="row">
            <div class="col-xs-12 video"></div>
        </div>
    </div>
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36600246-1']);
        _gaq.push(['_trackPageview']);
        
        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</body>
</html>
